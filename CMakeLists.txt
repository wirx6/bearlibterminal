cmake_minimum_required(VERSION 2.8)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_SKIP_RPATH ON)

if(WIN32)
	set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS ON)
	set(CMAKE_SUPPORT_WINDOWS_EXPORT_ALL_SYMBOLS ON)
endif()

option(BUILD_SAMPLES "Build samples" ON)
option(STATIC_DEPENDENCIES "Link dependencies statically" ON)

math(EXPR BITNESS "8*${CMAKE_SIZEOF_VOID_P}")

if(STATIC_DEPENDENCIES)
	set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static")
	set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -static")
endif()

add_subdirectory(Terminal)
if(BUILD_SAMPLES)
    add_subdirectory(Samples/Omni)
endif()
