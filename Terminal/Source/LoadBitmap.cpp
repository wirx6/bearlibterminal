/*
* BearLibTerminal
* Copyright (C) 2013 Cfyz
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
* of the Software, and to permit persons to whom the Software is furnished to do
* so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
* FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
* COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
* IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdexcept>
#include <cstring>
#include "LoadBitmap.hpp"
#include "Log.hpp"

#define STB_IMAGE_IMPLEMENTATION
#define STBI_MINGW_ENABLE_SSE2
#define STBI_FAILURE_USERMSG
#include <stb_image.h>

namespace BearLibTerminal
{
	Bitmap LoadBitmap(const std::vector<uint8_t>& data)
	{
		int width = 0, height = 0, comp = 0;
		unsigned char* bitmap = stbi_load_from_memory(&data[0], data.size(), &width, &height, &comp, STBI_rgb_alpha);

		if(bitmap == NULL){
			throw std::runtime_error(stbi_failure_reason());
		}

		LOG(Trace, L"Loaded image, " << width << L"x" << height);
		return Bitmap(Size(width, height), (Color*)bitmap);
	}
}
