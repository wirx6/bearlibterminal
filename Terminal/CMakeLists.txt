cmake_minimum_required(VERSION 2.8)

project(BearLibTerminal)

### OPTIONS

option(BUILD_SHARED_LIBS "Build dynamic version of library" ON)
option(USE_EMBEDDED_FT "Build dynamic version of library" ON)
set(VERSION_INFO "" CACHE STRING "Additional version info")

### DEPENDENCIES

find_package(OpenGL REQUIRED)
include_directories(${OPENGL_INCLUDE_DIRS})

if(USE_EMBEDDED_FT)
	add_subdirectory(Dependencies/FreeType)
	set(FREETYPE_LIBRARIES freetype2)
	include_directories(Dependencies/FreeType/Include)
else()
	find_package(Freetype REQUIRED)
	include_directories(${FREETYPE_INCLUDE_DIRS})
endif()

include_directories(Include/C)
include_directories(Dependencies/stb)

### SORCE

if(EXISTS "${CMAKE_SOURCE_DIR}/CHANGELOG.md")
	file(STRINGS "${CMAKE_SOURCE_DIR}/CHANGELOG.md" TERMINAL_VERSION_TOP_LINE LIMIT_COUNT 1)
	string(REGEX MATCH "[0-9\\.]+" TERMINAL_VERSION ${TERMINAL_VERSION_TOP_LINE})
else()
	set(TERMINAL_VERSION "Unknown")
endif()

if(NOT "${VERSION_INFO}" STREQUAL "")
	set(TERMINAL_VERSION "${TERMINAL_VERSION}-${VERSION_INFO}")
endif()


file(GLOB TERMINAL_HEADERS Source/*.hpp Source/*.h)
list(APPEND TERMINAL_HEADERS "${CMAKE_CURRENT_SOURCE_DIR}/Include/C/BearLibTerminal.h")
if(APPLE)
	file(GLOB TERMINAL_SOURCES Source/*.cpp Source/*.mm)
else()
	file(GLOB TERMINAL_SOURCES Source/*.cpp)
endif()

### FLAGS

if(WIN32)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DUNICODE")
endif()

if(NOT BUILD_SHARED_LIBS)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DBEARLIBTERMINAL_STATIC_BUILD")
endif()

### LIBRARY

add_library(BearLibTerminal ${TERMINAL_SOURCES} ${TERMINAL_HEADERS})
target_link_libraries(BearLibTerminal ${OPENGL_LIBRARIES} ${FREETYPE_LIBRARIES})

if(WIN32)
	target_link_libraries(BearLibTerminal winmm)
	target_link_libraries(BearLibTerminal "${CMAKE_CURRENT_SOURCE_DIR}/Resource/Terminal-${BITNESS}bit.res")
	set_target_properties(BearLibTerminal PROPERTIES PREFIX "")
elseif(APPLE)
	find_library(COCOA_LIBRARY Cocoa)
	target_link_libraries(BearLibTerminal ${COCOA_LIBRARY})
elseif(UNIX)
	target_link_libraries(BearLibTerminal pthread)
endif()

target_compile_definitions(BearLibTerminal PRIVATE TERMINAL_VERSION=\"${TERMINAL_VERSION}\")

### INSTALL

install(TARGETS BearLibTerminal
		RUNTIME DESTINATION bin
		LIBRARY DESTINATION bin
		ARCHIVE DESTINATION lib)
install(FILES ${TERMINAL_HEADERS} DESTINATION include/BearLibTerminal)
